#pragma once

#include <spdlog/spdlog.h>

#include <cstdlib>
#include <iostream>
#include <lyra/lyra.hpp>

class CmdArgs {
private:
    lyra::cli_parser _parser;

    bool _show_help{false};

    bool _verbose{false};

public:
    CmdArgs() {
        _parser |= lyra::help(_show_help) |
            lyra::opt(_verbose)["-v"]["--verbose"]("verbose output");
    }

    lyra::cli_parser& parser() { return _parser; }

    void parse(int argc, char** argv) {
        auto result = _parser.parse({argc, argv});
        if (!result) {
            std::cerr << result.errorMessage() << std::endl;
            std::cerr << _parser << std::endl;
            std::exit(1);
        }

        if (_show_help) {
            std::cout << _parser << std::endl;
            std::exit(0);
        }

        spdlog::default_logger()->set_pattern(
            "[%Y-%m-%d %H:%M:%S.%F] [%^%l%$] [%@] %v");
        if (_verbose) {
            spdlog::default_logger()->set_level(spdlog::level::trace);
        } else {
            spdlog::default_logger()->set_level(spdlog::level::info);
        }
    }
};
