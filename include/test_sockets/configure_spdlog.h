#pragma once

#include <spdlog/spdlog.h>

inline void configure_spdlog() {
    spdlog::default_logger()->set_level(spdlog::level::trace);
    spdlog::default_logger()->set_pattern(
        "[%Y-%m-%d %H:%M:%S.%F] [%^%l%$] [%@] %v");
}
