#include <benchmark/benchmark.h>

#include <asio.hpp>
#include <cstdint>
#include <iostream>
#include <string>

constexpr std::uint16_t port = 12345;

static void asio_send_receive(benchmark::State& state) {
    std::string sent_data;
    const std::size_t size = static_cast<std::size_t>(state.range());
    sent_data.reserve(size + 1);
    for (std::size_t i = 0; i < size; ++i) {
        sent_data.push_back('a');
    }
    sent_data.push_back('\n');

    std::string output_buffer;
    try {
        asio::io_context io_context;
        asio::ip::tcp::socket socket(io_context);
        socket.connect(asio::ip::tcp::endpoint(
            asio::ip::address::from_string("127.0.0.1"), port));

        for (auto _ : state) {
            asio::write(socket, asio::buffer(sent_data));

            output_buffer.clear();
            output_buffer.reserve(size + 1);
            asio::read_until(socket, asio::dynamic_buffer(output_buffer), '\n');
        }
    } catch (std::exception& e) {
        state.SkipWithError(e.what());
    }
}
BENCHMARK(asio_send_receive)->RangeMultiplier(32)->Range(1, 1024 * 1024);
