#include <spdlog/spdlog.h>

#include <asio.hpp>
#include <cstdint>
#include <future>
#include <iostream>
#include <string>

#include "test_sockets/cmd_args.h"

class Server {
public:
    asio::io_context _io_context{};

    asio::ip::tcp::acceptor _acceptor;

    std::future<void> _run_future{};

public:
    explicit Server(std::uint64_t port)
        : _acceptor(_io_context,
              asio::ip::tcp::endpoint(asio::ip::tcp::v4(), port)) {}

    void start() {
        _run_future = std::async([this] { run(); });
        accept_next();
    }

    void stop() {
        _io_context.stop();
        if (_run_future.valid()) {
            _run_future.get();
        }
    }

private:
    void run() { _io_context.run(); }

    void accept_next() {
        auto socket = std::make_shared<asio::ip::tcp::socket>(_io_context);
        _acceptor.async_accept(
            *socket, [this, socket](const asio::error_code& error) {
                process_connection(socket, error);
            });
    }

    void process_connection(std::shared_ptr<asio::ip::tcp::socket> socket,
        const asio::error_code& error) {
        if (error) {
            SPDLOG_ERROR("error on accept: {}", error.message());
        } else {
            SPDLOG_DEBUG("accepted an connection");

            auto input_buffer = std::make_shared<std::string>();
            input_buffer->reserve(1024 * 1024 + 1);
            read_next(socket, input_buffer);

            accept_next();
        }
    }

    void read_next(std::shared_ptr<asio::ip::tcp::socket> socket,
        std::shared_ptr<std::string> input_buffer) {
        asio::async_read_until(*socket, asio::dynamic_buffer(*input_buffer),
            '\n',
            [this, socket, input_buffer](
                const asio::error_code& error, std::size_t num_bytes) {
                handle_finished_read(socket, input_buffer, error, num_bytes);
            });
    }

    void handle_finished_read(std::shared_ptr<asio::ip::tcp::socket> socket,
        std::shared_ptr<std::string> input_buffer,
        const asio::error_code& error, std::size_t num_bytes) {
        if (error) {
            if (error == asio::error::eof) {
                SPDLOG_DEBUG("connection ended cleanly");
                return;
            }
            SPDLOG_ERROR("error on read: {}", error.message());
            return;
        }
        std::string line = input_buffer->substr(0, num_bytes - 1);
        SPDLOG_DEBUG("input: {}", line);
        line += '\n';
        asio::error_code write_error;
        asio::write(*socket, asio::buffer(line), write_error);
        if (write_error) {
            SPDLOG_ERROR("error on write: {}", write_error.message());
            return;
        }
        input_buffer->erase(0, num_bytes);
        read_next(socket, input_buffer);
    }
};

int main(int argc, char** argv) {
    CmdArgs parser;
    std::uint16_t port = 12345;
    parser.parser() |= lyra::opt(port, "num")["-p"]["--port"]("port number");
    parser.parse(argc, argv);

    SPDLOG_INFO("test asio server starts");

    try {
        SPDLOG_INFO("create a server on port {}", port);
        Server server(port);

        server.start();
        SPDLOG_INFO("waiting enter key");
        std::cin.ignore(10000, '\n');

        server.stop();
        SPDLOG_INFO("gracefull shutdown ended");
    } catch (std::exception& e) {
        SPDLOG_ERROR("exception thrown: {}", e.what());
        return 1;
    }

    return 0;
}
