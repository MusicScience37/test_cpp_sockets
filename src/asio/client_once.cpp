#include <spdlog/spdlog.h>

#include <asio.hpp>
#include <cstdint>
#include <string>

#include "test_sockets/cmd_args.h"

int main(int argc, char** argv) {
    CmdArgs parser;
    std::uint16_t port = 12345;
    parser.parser() |= lyra::opt(port, "num")["-p"]["--port"]("port number");
    parser.parse(argc, argv);

    SPDLOG_INFO("test asio client once starts");

    try {
        const std::string sent_data = "abc\n";

        asio::io_context io_context;
        asio::ip::tcp::socket socket(io_context);
        SPDLOG_INFO("connecting to port {}", port);
        socket.connect(asio::ip::tcp::endpoint(
            asio::ip::address::from_string("127.0.0.1"), port));

        SPDLOG_DEBUG("input: {}", sent_data.substr(0, sent_data.size() - 1));
        asio::write(socket, asio::buffer(sent_data));

        std::string output_buffer;
        asio::read_until(socket, asio::dynamic_buffer(output_buffer), '\n');

        SPDLOG_DEBUG(
            "output: {}", output_buffer.substr(0, output_buffer.size() - 1));

        SPDLOG_INFO("success");
    } catch (std::exception& e) {
        SPDLOG_ERROR("exception thrown: {}", e.what());
        return 1;
    }

    return 0;
}
