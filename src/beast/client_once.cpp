#include <spdlog/spdlog.h>

#include <boost/beast.hpp>
#include <cstdint>
#include <string>

#include "test_sockets/cmd_args.h"

int main(int argc, char** argv) {
    CmdArgs parser;
    std::uint16_t port = 12345;
    parser.parser() |= lyra::opt(port, "num")["-p"]["--port"]("port number");
    parser.parse(argc, argv);

    SPDLOG_INFO("test beast client once starts");

    try {
        const std::string sent_data = "abc";

        boost::asio::io_context io_context;
        boost::asio::ip::tcp::socket socket(io_context);
        SPDLOG_INFO("connecting to port {}", port);
        socket.connect(boost::asio::ip::tcp::endpoint(
            boost::asio::ip::address::from_string("127.0.0.1"), port));
        boost::beast::tcp_stream stream(std::move(socket));

        SPDLOG_DEBUG("input: {}", sent_data);
        boost::beast::http::request<boost::beast::http::string_body> request(
            boost::beast::http::verb::post, "/", 11, sent_data);
        request.content_length(sent_data.size());
        boost::beast::http::write(stream, request);

        boost::beast::http::response<boost::beast::http::string_body> response;
        boost::beast::flat_buffer buffer;
        boost::beast::http::read(stream, buffer, response);

        SPDLOG_DEBUG("output: {}", response.body());

        SPDLOG_INFO("success");
    } catch (std::exception& e) {
        SPDLOG_ERROR("exception thrown: {}", e.what());
        return 1;
    }

    return 0;
}
