#include <benchmark/benchmark.h>

#include <boost/beast.hpp>
#include <cstdint>
#include <string>

constexpr std::uint16_t port = 12345;

static void beast_send_receive(benchmark::State& state) {
    std::string sent_data;
    const std::size_t size = static_cast<std::size_t>(state.range());
    sent_data.reserve(size + 1);
    for (std::size_t i = 0; i < size; ++i) {
        sent_data.push_back('a');
    }

    try {
        boost::asio::io_context io_context;
        boost::asio::ip::tcp::socket socket(io_context);
        socket.connect(boost::asio::ip::tcp::endpoint(
            boost::asio::ip::address::from_string("127.0.0.1"), port));
        boost::beast::tcp_stream stream(std::move(socket));

        boost::beast::http::request<boost::beast::http::string_body> request(
            boost::beast::http::verb::post, "/", 11, sent_data);
        request.content_length(sent_data.size());
        request.keep_alive(true);

        for (auto _ : state) {
            boost::beast::http::write(stream, request);

            boost::beast::http::response<boost::beast::http::string_body>
                response;
            boost::beast::flat_buffer buffer;
            boost::beast::http::read(stream, buffer, response);
        }
    } catch (std::exception& e) {
        state.SkipWithError(e.what());
    }
}
BENCHMARK(beast_send_receive)->RangeMultiplier(32)->Range(1, 1024 * 1024);
