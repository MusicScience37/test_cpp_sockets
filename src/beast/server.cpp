#include <spdlog/spdlog.h>

#include <boost/beast.hpp>
#include <cstdint>
#include <future>
#include <iostream>
#include <memory>
#include <string>

#include "test_sockets/cmd_args.h"

class Session : public std::enable_shared_from_this<Session> {
private:
    boost::beast::tcp_stream _stream;

    boost::beast::flat_buffer _buffer{};

    boost::beast::http::request<boost::beast::http::string_body> _request{};

    boost::beast::http::response<boost::beast::http::string_body> _response{};

public:
    explicit Session(boost::asio::ip::tcp::socket&& socket)
        : _stream(std::move(socket)) {}

    void start() { read_next(); }

private:
    void read_next() {
        _request =
            boost::beast::http::request<boost::beast::http::string_body>();
        boost::beast::http::async_read(_stream, _buffer, _request,
            [session = shared_from_this()](
                const boost::system::error_code& error,
                std::size_t /*bytes_transferred*/) {
                session->on_read(error);
            });
    }

    void on_read(const boost::system::error_code& error) {
        if (error == boost::beast::http::error::end_of_stream) {
            SPDLOG_DEBUG("connection closed");
            close();
            return;
        }

        if (error) {
            SPDLOG_ERROR("error on read: {}", error.message());
            return;
        }

        SPDLOG_DEBUG("input: {}", _request.body());

        _response =
            boost::beast::http::response<boost::beast::http::string_body>(
                boost::beast::http::status::ok, _request.version(),
                _request.body());
        _response.content_length(_request.body().size());
        _response.keep_alive(_request.keep_alive());

        boost::beast::http::async_write(_stream, _response,
            [session = shared_from_this()](
                const boost::system::error_code& error,
                std::size_t /*bytes_transferred*/) {
                session->on_write(error);
            });
    }

    void on_write(const boost::system::error_code& error) {
        if (error) {
            SPDLOG_ERROR("error on write: {}", error.message());
            return;
        }

        SPDLOG_DEBUG("output: {}", _response.body());

        read_next();
    }

    void close() {
        boost::system::error_code error;
        _stream.socket().shutdown(
            boost::asio::ip::tcp::socket::shutdown_both, error);
        if (error) {
            SPDLOG_ERROR("error on close: {}", error.message());
        }
    }
};

class Server {
private:
    boost::asio::io_context _io_context{};

    boost::asio::ip::tcp::acceptor _acceptor;

    std::future<void> _run_future{};

public:
    explicit Server(std::uint64_t port)
        : _acceptor(_io_context,
              boost::asio::ip::tcp::endpoint(
                  boost::asio::ip::tcp::v4(), port)) {}

    void start() {
        _run_future = std::async([this] { run(); });
        accept_next();
    }

    void stop() {
        _io_context.stop();
        if (_run_future.valid()) {
            _run_future.get();
        }
    }

private:
    void run() { _io_context.run(); }

    void accept_next() {
        _acceptor.async_accept([this](const boost::system::error_code& error,
                                   boost::asio::ip::tcp::socket socket) {
            process_connection(error, std::move(socket));
        });
    }

    void process_connection(const boost::system::error_code& error,
        boost::asio::ip::tcp::socket socket) {
        if (error) {
            SPDLOG_ERROR("error on accept: {}", error.message());
        } else {
            SPDLOG_DEBUG("accepted an connection");

            auto input_buffer = std::make_shared<std::string>();
            auto session = std::make_shared<Session>(std::move(socket));
            session->start();

            accept_next();
        }
    }
};

int main(int argc, char** argv) {
    CmdArgs parser;
    std::uint16_t port = 12345;
    parser.parser() |= lyra::opt(port, "num")["-p"]["--port"]("port number");
    parser.parse(argc, argv);

    SPDLOG_INFO("test beast server starts");

    try {
        SPDLOG_INFO("create a server on port {}", port);
        Server server(port);

        server.start();
        SPDLOG_INFO("waiting enter key");
        std::cin.ignore(10000, '\n');

        server.stop();
        SPDLOG_INFO("gracefull shutdown ended");
    } catch (std::exception& e) {
        SPDLOG_ERROR("exception thrown: {}", e.what());
        return 1;
    }

    return 0;
}
