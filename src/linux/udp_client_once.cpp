#include <errno.h>        // errno
#include <netinet/in.h>   // IPv4
#include <netinet/udp.h>  // UDP in IPv4
#include <string.h>       // strerror
#include <sys/socket.h>   // socket
#include <sys/types.h>    // needed on some platforms
#include <unistd.h>       // close

#include "test_sockets/cmd_args.h"

int main(int argc, char** argv) {
    CmdArgs parser;
    std::uint16_t port = 12345;
    parser.parser() |= lyra::opt(port, "num")["-p"]["--port"]("port number");
    parser.parse(argc, argv);

    SPDLOG_INFO("test Linux UDP client once starts");

    const std::string sent_data = "abc";

    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    try {
        SPDLOG_INFO("send to port {}", port);
        sockaddr_in addr;
        addr.sin_family = AF_INET;
        addr.sin_port = htons(port);
        addr.sin_addr.s_addr = INADDR_ANY;

        SPDLOG_DEBUG("input: {}", sent_data);
        ssize_t send_size = sendto(sock, sent_data.data(), sent_data.size(), 0,
            (sockaddr*)(&addr), sizeof(sockaddr));
        if (send_size == -1) {
            throw std::runtime_error(
                "sendto failed with " + std::string(strerror(errno)));
        }

        std::vector<char> buf(65536);
        ssize_t read_size = recv(sock, buf.data(), buf.size(), 0);
        if (read_size == -1) {
            throw std::runtime_error(
                "recv failed with " + std::string(strerror(errno)));
        }
        SPDLOG_DEBUG("output: {}",
            std::string(buf.data(), static_cast<std::size_t>(read_size)));
    } catch (std::exception& e) {
        SPDLOG_ERROR("exception thrown: {}", e.what());
    }
    close(sock);
    SPDLOG_INFO("gracefull shutdown ended");

    return 0;
}
