#include <errno.h>        // errno
#include <netinet/in.h>   // IPv4
#include <netinet/udp.h>  // UDP in IPv4
#include <poll.h>         // poll
#include <string.h>       // strerror
#include <sys/socket.h>   // socket
#include <sys/types.h>    // needed on some platforms
#include <unistd.h>       // close

#include <atomic>
#include <iostream>
#include <thread>

#include "test_sockets/cmd_args.h"

int main(int argc, char** argv) {
    CmdArgs parser;
    std::uint16_t port = 12345;
    parser.parser() |= lyra::opt(port, "num")["-p"]["--port"]("port number");
    parser.parse(argc, argv);

    SPDLOG_INFO("test Linux UDP server starts");

    std::atomic<bool> enabled{true};
    std::thread th([&enabled] {
        std::cin.ignore(10000, '\n');
        enabled = false;
    });

    SPDLOG_INFO("create a server on port {}", port);
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    try {
        sockaddr_in addr;
        addr.sin_family = AF_INET;
        addr.sin_port = htons(port);
        addr.sin_addr.s_addr = INADDR_ANY;
        if (bind(sock, (sockaddr*)(&addr), sizeof(addr)) != 0) {
            throw std::runtime_error(
                "bind failed with " + std::string(strerror(errno)));
        }

        while (enabled) {
            pollfd events;
            events.fd = sock;
            events.events = POLLIN;
            events.revents = 0;
            constexpr int timeout_ms = 100;
            int poll_res = poll(&events, 1, timeout_ms);
            if (poll_res == 0) {
                // timeout
                continue;
            } else if (poll_res == -1) {
                throw std::runtime_error(
                    "poll failed with " + std::string(strerror(errno)));
            }

            std::vector<char> buf(65536);
            sockaddr_in addr;
            socklen_t addr_size = sizeof(addr);
            ssize_t read_size = recvfrom(sock, buf.data(), buf.size(), 0,
                (sockaddr*)(&addr), &addr_size);
            if (read_size == -1) {
                throw std::runtime_error(
                    "recvfrom failed with " + std::string(strerror(errno)));
            }
            SPDLOG_DEBUG("received {}",
                std::string(buf.data(), static_cast<std::size_t>(read_size)));

            ssize_t send_size =
                sendto(sock, buf.data(), static_cast<size_t>(read_size), 0,
                    (sockaddr*)(&addr), addr_size);
            if (send_size == -1) {
                throw std::runtime_error(
                    "sendto failed with " + std::string(strerror(errno)));
            }
            SPDLOG_DEBUG("sended response");
        }
    } catch (std::exception& e) {
        SPDLOG_ERROR("exception thrown: {}", e.what());
    }
    close(sock);
    SPDLOG_INFO("gracefull shutdown ended");

    th.join();

    return 0;
}
