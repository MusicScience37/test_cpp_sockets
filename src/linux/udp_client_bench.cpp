#include <benchmark/benchmark.h>
#include <errno.h>        // errno
#include <netinet/in.h>   // IPv4
#include <netinet/udp.h>  // UDP in IPv4
#include <string.h>       // strerror
#include <sys/socket.h>   // socket
#include <sys/types.h>    // needed on some platforms
#include <unistd.h>       // close

constexpr std::uint16_t port = 12345;

static void udp_send_receive(benchmark::State& state) {
    std::string sent_data;
    const std::size_t size = static_cast<std::size_t>(state.range());
    sent_data.reserve(size + 1);
    for (std::size_t i = 0; i < size; ++i) {
        sent_data.push_back('a');
    }
    std::vector<char> buf(65536);

    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    try {
        for (auto _ : state) {
            sockaddr_in addr;
            addr.sin_family = AF_INET;
            addr.sin_port = htons(port);
            addr.sin_addr.s_addr = INADDR_ANY;

            ssize_t send_size = sendto(sock, sent_data.data(), sent_data.size(),
                0, (sockaddr*)(&addr), sizeof(sockaddr));
            if (send_size == -1) {
                throw std::runtime_error(
                    "sendto failed with " + std::string(strerror(errno)));
            }

            ssize_t read_size = recv(sock, buf.data(), buf.size(), 0);
            if (read_size == -1) {
                throw std::runtime_error(
                    "recv failed with " + std::string(strerror(errno)));
            }
        }
    } catch (std::exception& e) {
        state.SkipWithError(e.what());
    }
    close(sock);
}
BENCHMARK(udp_send_receive)->RangeMultiplier(32)->Range(1, 32 * 1024);
