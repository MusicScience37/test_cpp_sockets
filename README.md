# test_cpp_sockets

Test of sockets in C++

## Build

This project uses CMake,
so one can build this project as follows:

```console
cd path/to/this/directory
mkdir build
cd build
cmake ..
cmake --build .
```
